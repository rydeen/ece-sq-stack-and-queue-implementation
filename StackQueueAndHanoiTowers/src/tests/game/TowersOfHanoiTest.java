package tests.game;

import org.junit.*;
import static org.junit.Assert.*;

import implementation.stack.Stack;
import implementation.exceptions.EmptyTowerException;
import implementation.exceptions.IllegalStateException;
import implementation.game.TowersOfHanoi;


public class TowersOfHanoiTest {
	
	
	TowersOfHanoi game ;
	int numberOfRings = 3 ;
	int source ;
	int destination ;
	int via ;
	
	
	@Test
	public void test_Contructor() throws IllegalStateException {
		// tests all the values after the creation of the game
		
		// test using all 3 towers as source
		for ( int tower=0 ; tower<3 ; tower++) {
		
			// create the game
			this.game = new TowersOfHanoi( numberOfRings , tower ) ;
			
			// check the number of towers created
			assertEquals( 3 , this.game.getTowers().length ) ;
			
			// verify the number of rings
			assertEquals( this.numberOfRings , this.game.getNumRings() ) ;
			
			// the source tower is the one who should be full

			for ( int i=0 ; i<3 ; i++ ) {
				if ( tower==i )
					assertEquals( true , this.game.getTowers()[i].rings.isFull() ) ;
				else
					assertEquals( true , this.game.getTowers()[i].rings.isEmpty() ) ;
			}
			
			// verify the content of the source tower
			Stack<Integer> sourceTower = this.game.getTowers()[tower].rings ;
			for ( int ring=1 ; ring<=this.numberOfRings ; ring++ )
				assertEquals( ring , (int)sourceTower.pop() ) ;
		}
	}
	
	@Test ( expected = IllegalArgumentException.class )
	public void test_ConstructorException_IllegalNumRings() {
		// exception is thrown because of the number of rings
		this.game = new TowersOfHanoi(0,0) ;
		this.game = new TowersOfHanoi(-1,0) ;
	}
	
	@Test ( expected = IllegalArgumentException.class )
	public void test_ConstructorException_IllegalSource() {
		// exception is throw because of the source tower index
		this.game = new TowersOfHanoi(3,3) ;
		this.game = new TowersOfHanoi(3,-1) ;
	}
	
	@Test (expected = EmptyTowerException.class )
	public void test_StartMethod_ExceptionEmpty() throws ArrayIndexOutOfBoundsException, EmptyTowerException {
		this.source = 0 ;
		this.via = 1 ;
		this.destination = 2 ;
		
		this.game = new TowersOfHanoi( this.numberOfRings , this.source ) ;
		
		assertEquals( true , this.source==0) ;
		assertEquals( true , this.source!=1) ;
		
		// test using a different tower has source (exception should be thrown)
		this.game.start( 1 , this.destination , this.via ) ;
	}
	
	@Test (expected = ArrayIndexOutOfBoundsException.class )
	public void test_StartMethod_ExceptionOutOfBounds_1() throws ArrayIndexOutOfBoundsException, EmptyTowerException {
		this.source = 0 ;
		this.via = -1 ;
		this.destination = -1 ;
		
		this.game = new TowersOfHanoi( this.numberOfRings , this.source ) ;
		
		assertEquals( false , this.source<0) ; // we need source to be valid to create the game above
		assertEquals( true , this.via<0) ;
		assertEquals( true , this.destination<0) ;
		
		// test using a different tower has source (exception should be thrown)
		this.game.start( this.source , this.destination , this.via ) ;
	}
	
	@Test (expected = ArrayIndexOutOfBoundsException.class )
	public void test_StartMethod_ExceptionOutOfBounds_2() throws ArrayIndexOutOfBoundsException, EmptyTowerException {
		this.source = 0 ;
		this.via = 3 ;
		this.destination = 3 ;
		
		this.game = new TowersOfHanoi( this.numberOfRings , this.source ) ;
		
		assertEquals( false , this.source>2) ; // we need source to be valid to create the game above
		assertEquals( true , this.via>2) ;
		assertEquals( true , this.destination>2) ;
		
		// test using a different tower has source (exception should be thrown)
		this.game.start( this.source , this.destination , this.via ) ;
	}
	
	@Test
	public void test_Start() throws ArrayIndexOutOfBoundsException, EmptyTowerException, IllegalStateException {
		this.source = 0 ;
		this.destination  = 1 ;
		this.via = 2 ;
		
		this.game = new TowersOfHanoi( this.numberOfRings , this.source ) ;
		this.game.start( this.source , this.destination , this.via ) ;
		
		// verify the results :: the destination tower must be the only one full !
		for ( int i=0 ; i<3 ; i++ )
			if ( i==this.destination)
				assertEquals( true , this.game.getTowers()[i].rings.isFull() ) ;
			else
				assertEquals( true , this.game.getTowers()[i].rings.isEmpty() ) ;
		
		// verify the content of the destination tower
		Stack<Integer> results = this.game.getTowers()[this.destination].rings ;
		Stack<Integer> resultsMethod = this.game.getFinalResults() ;
		
		// verify both stack are equal
		assertEquals( results.getSize() , resultsMethod.getSize() ) ;
		assertEquals( results.isFull() , resultsMethod.isFull() ) ;
		assertEquals( results.isEmpty() , resultsMethod.isEmpty() ) ;
		assertEquals( results.getNumberOfElements() , resultsMethod.getNumberOfElements() ) ;
		
		// verify the content in both stacks is the same
		// verify the values are ordered in a correct order
		// [note >>> results and resultsMethod should reference the same data]
		for ( int v=1 ; v<=this.numberOfRings ; v++) {
			assertEquals( v , (int)results.head() ) ;
			assertEquals( v , (int)resultsMethod.head() ) ;
			results.pop() ;
		}
		
		// verify both stacks are empty - no value was left
		assertEquals( true , results.isEmpty() ) ;
		assertEquals( true , resultsMethod.isEmpty() ) ;
	}
}