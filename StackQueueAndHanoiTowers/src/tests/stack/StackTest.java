package tests.stack;

import org.junit.*;
import static org.junit.Assert.*;

import implementation.stack.Stack;
import implementation.exceptions.IllegalStateException;;

public class StackTest {

	private final int stackMaxSize = 6 ;
	
	Stack<Integer> stack ;
	Integer i1 ;
	Integer i2 ;
	Integer i3 ;
	Integer i4 ;
	Integer i5 ;
	Integer i6 ;
	Integer i7 ;
	Integer i8 ;
	
	@Before
	public void setUp() {
		this.stack = new Stack<Integer>( stackMaxSize ) ;
		this.i1 = new Integer(1) ;
		this.i2 = new Integer(2) ;
		this.i3 = new Integer(3) ;
		this.i4 = new Integer(4) ;
		this.i5 = new Integer(5) ;
		this.i6 = new Integer(6) ;
		this.i7 = new Integer(7) ;
		this.i8 = new Integer(8) ;
	}
	
	@After
	public void tearDown() {
		this.stack = null ;
		this.i1 = null ;
		this.i2 = null ;
		this.i3 = null ;
		this.i4 = null ;
		this.i5 = null ;
		this.i6 = null ;
		this.i7 = null ;
		this.i8 = null ;
	}


	// test after stack creation
	//  - getSize() = stackMaxSize
	@Test
	public void test_StackConstructor() {
		assertEquals(stackMaxSize, stack.getSize());
	}

	// tests with stack empty
	//  - exception when pop()
	//  - exception when head()
	//  - isEmpty() == true
	//  - getNumberOfElems() == 0
	@Test
	public void test_StackIsEmpty() {
		assertEquals(0, stack.getNumberOfElements() );
		assertEquals( true , stack.isEmpty() );
	}
	
	@Test ( expected = IllegalStateException.class )
	public void test_StackExceptionEmpty_pop() throws IllegalStateException {
		stack.pop() ;
	}
	
	@Test ( expected = IllegalStateException.class )
	public void test_StackExceptionEmpty_head() throws IllegalStateException {
		stack.head();
	}

	// test stack add  and remove elements
	//  - add i1, i2 and then check head()==i2
	//  - check getNumberOfElements()==2
	//  - check pop()==i2
	//  - check getNumberOfElements()==1 and head()==i1
	@Test
	public void test_StackPush() throws IllegalStateException {
		assertEquals( true, stack.isEmpty() );
		stack.push(i1);
		stack.push(i2);
		assertEquals( 2 , stack.getNumberOfElements() );
		assertEquals( false, stack.isEmpty());
		assertEquals( false, stack.isFull());
	}
	
	@Test
	public void test_StackHead() throws IllegalStateException {
		stack.push(i1);
		stack.push(i2);
		assertEquals( i2, stack.head() );
		assertEquals( 2 , stack.getNumberOfElements() );
	}

	@Test
	public void test_StackPop() throws IllegalStateException {
		stack.push(i1);
		stack.push(i2);
		Integer temp1 = stack.pop();
		assertEquals( i2 , temp1 );
		assertEquals( i1 , stack.head() );
		assertEquals( 1 , stack.getNumberOfElements() );
		
		Integer temp2 = stack.pop();
		assertEquals( true, stack.isEmpty() );
		assertEquals( 0, stack.getNumberOfElements());
		assertEquals( i1 , temp2 );
	}


	// test stack state after a group of actions
	@Test
	public void test_StackSetOfOperations() throws IllegalStateException {
		// starts empty
		assertEquals(0,stack.getNumberOfElements());
		
		// adding ...
		stack.push(i1);
		stack.push(i2);
		stack.push(i3);
		assertEquals(i3, stack.head());
		assertEquals(3,stack.getNumberOfElements());
		
		// removing ...
		stack.pop();
		stack.pop();
		assertEquals(i1, stack.head());
		assertEquals(1, stack.getNumberOfElements());
		
		// adding again ...
		stack.push(i4);
		stack.push(i5);
		stack.push(i6);
		assertEquals(i6,stack.head());
		assertEquals(4,stack.getNumberOfElements());
		
		// make stack full
		stack.push(i7);
		stack.push(i8);
		
		// ends full
		assertEquals(6,stack.getNumberOfElements());
		assertEquals( true, stack.isFull() );
	}


	// tests with stack full
	//  - isFull() == true
	@Test
	public void test_StackIsFull() throws IllegalStateException {
		stack.push(i1);
		stack.push(i2);
		stack.push(i3);
		stack.push(i4);
		stack.push(i5);
		stack.push(i6);
		assertEquals( true, stack.isFull() );
		assertEquals( stackMaxSize, stack.getNumberOfElements() );
	}

	@Test ( expected = IllegalStateException.class )
	public void test_StackExceptionFull() throws IllegalStateException {
		stack.push(i1);
		stack.push(i2);
		stack.push(i3);
		stack.push(i4);
		stack.push(i5);
		stack.push(i6);
		stack.push(i7); // this should cause an exception
	}
}