package tests.queue;

import org.junit.*;
import static org.junit.Assert.*;

import implementation.queue.Queue;
import implementation.exceptions.IllegalStateException;

public class QueueTest {

	private final int queueMaxSize = 6 ;

	Queue<Integer> queue ;
	Integer i1 ;
	Integer i2 ;
	Integer i3 ;
	Integer i4 ;
	Integer i5 ;
	Integer i6 ;
	Integer i7 ;
	Integer i8 ;
	
	@Before
	public void setUp() {
		this.queue = new Queue<Integer>( queueMaxSize ) ;
		this.i1 = new Integer(1) ;
		this.i2 = new Integer(2) ;
		this.i3 = new Integer(3) ;
		this.i4 = new Integer(4) ;
		this.i5 = new Integer(5) ;
		this.i6 = new Integer(6) ;
		this.i7 = new Integer(7) ;
		this.i8 = new Integer(8) ;
	}

	@After
	public void tearDown() {
		this.queue = null ;
		this.i1 = null ;
		this.i2 = null ;
		this.i3 = null ;
		this.i4 = null ;
		this.i5 = null ;
		this.i6 = null ;
		this.i7 = null ;
		this.i8 = null ;
	}


	// test after queue creation
	//  - getSize() = queueMaxSize
	@Test
	public void test_QueueConstructor() {
		assertEquals(queueMaxSize, queue.getSize());
	}
	
	// tests with queue empty
	//  - exception when pop()
	//  - exception when head()
	//  - isEmpty() == true
	//  - getNumberOfElems() == 0
	@Test
	public void test_QueueIsEmpty() {
		assertEquals(0, queue.getNumberOfElements() );
		assertEquals( true , queue.isEmpty() );
	}
	
	@Test ( expected = IllegalStateException.class )
	public void test_QueueExceptionEmpty_pop() throws IllegalStateException {
		queue.pop() ;
	}
	
	@Test ( expected = IllegalStateException.class )
	public void test_QueueExceptionEmpty_head() throws IllegalStateException {
		queue.head();
	}
	
	// test queue add and remove elements
	//  - add i1, i2 and then check head()==i1
	//  - check getNumberOfElements()==2
	//  - check pop()==i1
	//  - check getNumberOfElements()==1 and head()==i2
	@Test
	public void test_QueuePush() throws IllegalStateException {
		assertEquals( true, queue.isEmpty() );
		queue.push(i1);
		queue.push(i2);
		assertEquals( 2 , queue.getNumberOfElements() );
		assertEquals( false, queue.isEmpty());
		assertEquals( false, queue.isFull());
	}
	
	@Test
	public void test_QueueHead() throws IllegalStateException {
		queue.push(i1);
		queue.push(i2);
		assertEquals( i1, queue.head() );
		assertEquals( 2 , queue.getNumberOfElements() );
	}
	
	@Test
	public void test_QueuePop() throws IllegalStateException {
		queue.push(i1);
		queue.push(i2);
		
		Integer temp1 = queue.pop();
		assertEquals( i1 , temp1 );
		assertEquals( i2 , queue.head() );
		assertEquals( 1 , queue.getNumberOfElements() );
		
		Integer temp2 = queue.pop();
		assertEquals( true, queue.isEmpty() );
		assertEquals( 0, queue.getNumberOfElements());
		assertEquals( i2 , temp2 );
	}
	
	
	// test queue state after a group of actions
	@Test
	public void test_QueueSetOfOperations() throws IllegalStateException {
		// starts empty
		assertEquals(0,queue.getNumberOfElements());
		
		// adding ...
		queue.push(i1);
		queue.push(i2);
		queue.push(i3);
		assertEquals(i1, queue.head());
		assertEquals(3,queue.getNumberOfElements());
		
		// removing ...
		queue.pop();
		queue.pop();
		assertEquals(i3, queue.head());
		assertEquals(1, queue.getNumberOfElements());
		
		// adding again ...
		queue.push(i4);
		queue.push(i5);
		queue.push(i6);
		assertEquals(i3,queue.head());
		assertEquals(4,queue.getNumberOfElements());
		
		// make queue full
		queue.push(i7);
		queue.push(i8);
		
		// ends full
		assertEquals(6,queue.getNumberOfElements());
		assertEquals( true, queue.isFull() );
	}
	
	
	// tests with queue full
	//  - isFull() == true
	@Test
	public void test_QueueIsFull() throws IllegalStateException {
		queue.push(i1);
		queue.push(i2);
		queue.push(i3);
		queue.push(i4);
		queue.push(i5);
		queue.push(i6);
		
		assertEquals( true, queue.isFull() );
		assertEquals( queueMaxSize, queue.getNumberOfElements() );
	}
	
	@Test ( expected = IllegalStateException.class )
	public void test_QueueExceptionFull() throws IllegalStateException {
		queue.push(i1);
		queue.push(i2);
		queue.push(i3);
		queue.push(i4);
		queue.push(i5);
		queue.push(i6);
		queue.push(i7); // this should cause the exception
	}
}