package implementation.queue;

import implementation.exceptions.IllegalStateException;
import implementation.stack.*;

/**
 * 
 * @author Luis Abreu
 *
 * @param <T>
 */
public class Queue<T> { 

	private Stack<T> stack1 ;
	private Stack<T> stack2 ;
	
	/**
	 * Queue constructor. Creates a new queue with the maximum size of 'size'
	 * @param size
	 */
	public Queue( int size ) {
		this.stack1 = new Stack<T>( size ) ;
		this.stack2 = new Stack<T>( size ) ;
	}
	
	/**
	 * Adds a new value to the queue.
	 * @param value
	 * @throws IllegalStateException if the queue is already full
	 */
	public void push(T value) throws IllegalStateException {
		this.stack1.push( value ) ;
	}
	
	/**
	 * @return the first element added to the queue, removing it.
	 * @throws IllegalStateException if the queue is empty
	 */
	public T pop() throws IllegalStateException {
		if ( this.stack2.isEmpty() )
			this.moveAllValuesFromStack1ToStack2();
		
		return this.stack2.pop() ;
	}
	
	/**
	 * @return the first element added to the queue without removing it from the queue.
	 * @throws IllegalStateException if the queue is empty.
	 */
	public T head() throws IllegalStateException {
		if ( this.stack2.isEmpty() )
			this.moveAllValuesFromStack1ToStack2();
		
		return this.stack2.head() ;
	}
	
	/**
	 * @return true if the queue is full or false otherwise
	 */
	public boolean isFull() {
		return this.stack1.isFull() || this.stack2.isFull() || this.getNumberOfElements()==this.stack1.getSize() ;
	}
	
	/**
	 * @return true if the queue is empty or false otherwise
	 */
	public boolean isEmpty() {
		return this.stack1.isEmpty() && this.stack2.isEmpty() ;
	}
	
	/**
	 * @return the size of the queue
	 */
	public int getSize() {
		return this.stack1.getSize() ;
	}
	
	/**
	 * @return the number of elements inside the queue
	 */
	public int getNumberOfElements() {
		return this.stack1.getNumberOfElements() + this.stack2.getNumberOfElements() ;
	}

	/**
	 * Transfers all the content from the first stack to the second.
	 * @throws IllegalStateException 
	 */
	private void moveAllValuesFromStack1ToStack2() throws IllegalStateException {
		while ( ! this.stack1.isEmpty() )
			this.stack2.push( this.stack1.pop() ) ;
	}
}