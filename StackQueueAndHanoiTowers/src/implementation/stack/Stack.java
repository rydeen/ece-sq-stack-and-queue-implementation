package implementation.stack;

import implementation.exceptions.IllegalStateException ;

/**
 * 
 * @author Luis Abreu
 *
 * @param <T>
 */
public class Stack<T> {

	private T[] array;
    private int top; 


    /**
     * Stack constructor. Creates a new stack with the maximum size of 'size'
     * @param size - max size of the stack
     */
     @SuppressWarnings("unchecked")
	public Stack(int size) { 
         this.array = (T[]) new Object[size];
         top = -1; 
     }

    /**
     * Adds a new value to the stack
     * @param value - value to be added
     * @throws IllegalStateException if the stack is already full
     */
     public void push(T value) throws IllegalStateException { 
          if ( ! this.isFull() )
               this.array[++top] = value;
          else
               throw new IllegalStateException() ;
     }

    /**
     * @returns the value on top of the stack and removes it
     * @throws IllegalStateException if the stack is empty
     */
     public T pop() throws IllegalStateException { 
    	 if ( this.top == -1 )
    		 throw new IllegalStateException() ;

    	  return this.array[top--];
     }

    /**
     * @returns the top value of the stack without removing it
     * @throws IllegalStateException if the stack is empty
     */
     public T head() throws IllegalStateException { 
    	 if ( this.top == -1 )
    		 throw new IllegalStateException() ;
    	 
          return this.array[top];
     }

    /**
     * @returns true if the stack is empty or false otherwise
     */
     public boolean isEmpty() { 
          return (top == -1); 
     } 

    /**
     * @returns true if the stack is full or false otherwise
     */
     public boolean isFull() { 
          return (this.top == this.array.length - 1); 
     }

    /**
     * @returns the size of the stack
     */
     public int getSize() {
          return this.array.length;
     }
     
    /**
     * @returns the number of elements in the stack
     */
     public int getNumberOfElements() {
    	 return this.top+1 ;
     }
}