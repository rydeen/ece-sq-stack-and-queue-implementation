package implementation.game;

import java.lang.reflect.Array;

import implementation.exceptions.EmptyTowerException;
import implementation.exceptions.IllegalStateException;
import implementation.stack.Stack;

public class TowersOfHanoi {

    private int n_rings;
    private int destination ;
    private Tower[] towers;
    
    /**
     * Main function to create and solve the game. Need to specify the number of rings to use in the game
     * @param args
     * @throws ArrayIndexOutOfBoundsException if negative number argument
     * @throws IllegalArgumentException if the number of arguments is different from 1
     * @throws NumberFormatException if the argument is not a number
     */
    public static void main( String[] args ) 
			throws ArrayIndexOutOfBoundsException, IllegalArgumentException, NumberFormatException {	
		
		if ( args.length != 1 ) 
			throw new IllegalArgumentException() ;
			
		
		int numberOfRings = Integer.parseInt(args[0]) ; // get from args after!!
		int source = 0 ;
		int destination = 1 ;
		int via = 2 ;
	
		TowersOfHanoi game = new TowersOfHanoi( numberOfRings , source ) ;
		try {
			game.start(source,destination,via) ;
		} catch (EmptyTowerException e) {
			// will never happen because the value for the source tower is the same used in the constructor
		}
		
	}
    
    
    /**
     * Class constructor
     * @param n_rings
     * @param sourceTower
     * @throws IllegalArgumentException if the sourceTower is an invalid index ( != 0,1,2 ) or if n_rings<1
     */
    public TowersOfHanoi( int n_rings, int sourceTower ) throws IllegalArgumentException {
    	
    	if ( sourceTower > 2 || sourceTower < 0  || n_rings < 1 )
    		throw new IllegalArgumentException() ;
    	
    	this.n_rings = n_rings ;
    	this.towers = (Tower[])Array.newInstance(Tower.class, 3) ;
    	
    	for ( int i=0; i<this.towers.length; i++ )
    		this.towers[i] = new Tower( this.n_rings ) ;
    	
    	for ( int i=n_rings ; i>=1 ; i-- )
			try {
				this.towers[sourceTower].rings.push( i ) ;
			} catch (IllegalStateException e) {
				// this should never happen ... it will push n times, where n=stack max size
			}
    }
    
    /**
     * @return the number of rings used in game
     */
    public int getNumRings() {
    	return this.n_rings ;
    }
    
    /**
     * @return the index of the destination tower
     */
    public int getDestination() {
    	return this.destination ;
    }
    
    /**
     * @return an array with the 3 towers of the game
     */
    public Tower[] getTowers() {
    	return this.towers ;
    }
    

    /**
     * Starts the game solver
     * @param sourceTower
     * @param destinationTower
     * @param viaTower
     * @throws ArrayIndexOutOfBoundsException if any of the arguments is negative or above 3
     * @throws EmptyTowerException if sourceTower is empty
     */
    public void start( int sourceTower , int destinationTower, int viaTower ) 
    		throws ArrayIndexOutOfBoundsException, EmptyTowerException {
    	
    	if ( sourceTower < 0 || destinationTower < 0 || viaTower < 0 ||
    			sourceTower > 2 || destinationTower > 2 || viaTower > 2  )
    		throw new ArrayIndexOutOfBoundsException() ;
    	
    	if ( this.towers[sourceTower].rings.isEmpty() )
    		throw new EmptyTowerException() ;
    	else {
    		this.destination = destinationTower ;
    		
    		printMove() ; // print initial state
    		
    		move( this.n_rings , this.towers[sourceTower] , this.towers[destinationTower] , this.towers[viaTower]) ;
    	}
    }
    
    /**
     * Move a ring from one tower to another
     * @param rings
     * @param source
     * @param destination
     * @param via
     */
    public void move( int rings , Tower source, Tower destination , Tower via ) {
    	
    	try {
    		
	    	if ( rings == 1 ) {
	    		destination.rings.push( source.rings.pop() ) ;
	    		printMove();
	    	}
	    	else {
	    		move( rings-1 , source, via, destination) ;
	    		destination.rings.push( source.rings.pop() ) ;
	    		printMove();
	    		move( rings-1 , via, destination , source ) ;
	    	}
	    	
    	} catch( IllegalStateException e ){
			// will never happen
		}
    }
    
    /**
     * @return implementation.stack.Stack Object, containing the elements in the destination tower,
     * which should have all the rings in the correct order
     */
    public Stack<Integer> getFinalResults() {
    	return this.towers[this.destination].rings ;
    }

    /**
     * Prints the current step of the game using the System.out call
     */
    private void printMove() {
    	
    	/* prints the first line :: always the same */
    	for( int k=0 ; k<this.towers.length ; k++ ) {
    		for ( int i=0 ; i<((this.n_rings*2)+2) ; i++ )
        		System.out.print(" ");
    		
        	System.out.print(" _ ") ;
    	}
    	
    	System.out.print("\n");
    	
    	/* print towers content */
    	for ( int i=this.n_rings ; i>0 ; i-- ) {
    		
    		int spacesOccupiedByRings = 0 ;
    		
    		for ( int t=0 ; t<this.towers.length ; t++ ) {
    			
    			if ( this.towers[t].rings.getNumberOfElements() > i ) { // print ring (its in the somewhere in the stack)
    				
    				try {
    				
	    				Stack<Integer> stack_aux = new Stack<Integer>( this.towers[t].rings.getSize() ) ;
	    				int popNtimes = this.towers[t].rings.getNumberOfElements()-i;
	    				
	    				for ( int s=0 ; s<popNtimes ; s++ ) {
	    					int elemExtracted = this.towers[t].rings.pop() ;
	    					stack_aux.push( elemExtracted );
	    				}
	    				
	    				int ring = this.towers[t].rings.head() ;
	    				
	    				while( ! stack_aux.isEmpty() ) {
	    					int elemExtracted = stack_aux.pop() ;
	    					this.towers[t].rings.push( elemExtracted );
	    				}
	    				
	    				for ( int m=0 ; m<((this.n_rings*2)+2)-ring-spacesOccupiedByRings ; m++ )
			        		System.out.print(" ");
	    				
	    				for ( int p=0 ; p<ring ; p++ )
	    					System.out.print("+");
	    				
	    		        System.out.print("| |") ;
	    		        
	    		        for ( int p=0 ; p<ring ; p++ )
	    					System.out.print("+");
	    		        
	    		        spacesOccupiedByRings = ring ;
    		        
    				} catch( IllegalStateException e ){
						// will never happen
					}
    				
    			}
    			else if ( this.towers[t].rings.getNumberOfElements() == i ) { // print ring (its the head of the stack)
    				
    				try {
    				
	    				int ring = this.towers[t].rings.head() ;
	    				
	    				for ( int m=0 ; m<((this.n_rings*2)+2)-ring-spacesOccupiedByRings ; m++ )
			        		System.out.print(" ");
	    				
	    				for ( int p=0 ; p<ring ; p++ )
	    					System.out.print("+");
	    				
	    		        System.out.print("| |") ;
	    		        
	    		        for ( int p=0 ; p<ring ; p++ )
	    					System.out.print("+");
	    		        
	    		        spacesOccupiedByRings = ring ;
    		        
    				} catch( IllegalStateException e ){
						// will never happen
					}
    			}
    			else { // print empty line
    				
		    		for ( int m=0 ; m<((this.n_rings*2)+2)-spacesOccupiedByRings ; m++ )
		        		System.out.print(" ");
		    		
		        	System.out.print("| |") ;
		        	
		        	spacesOccupiedByRings = 0 ;
    			}
    			
    		}
    		
    		System.out.print("\n");
    	}
    	
    	
    	/* prints the bottom :: always the same */
    	for ( int i=0 ; i<((this.n_rings*2)+2) ; i++ )
    		System.out.print("/");
    	System.out.print("///");
    	for ( int i=0 ; i<((this.n_rings*2)+2) ; i++ )
    		System.out.print("/");
    	System.out.print("/ \\");
    	for ( int i=0 ; i<((this.n_rings*2)+2) ; i++ )
    		System.out.print("\\");
    	System.out.print("\\\\\\");
    	for ( int i=0 ; i<((this.n_rings*2)+2) ; i++ )
    		System.out.print("\\");
    	
    	
    	// creates a space between this and the possible next print
    	System.out.print("\n\n");
    }

}